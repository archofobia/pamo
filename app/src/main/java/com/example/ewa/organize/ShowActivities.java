package com.example.ewa.organize;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import adapters.TaskAdapter;
import models.Task;

public class ShowActivities extends AppCompatActivity {
    List<Task> activityList;
    SQLiteDatabase database;
    ListView listView;
    TaskAdapter adapter;
    ArrayAdapter<String> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_activities);
        database = openOrCreateDatabase(MainActivity.DATABASE_NAME, MODE_PRIVATE, null);

        listView = (ListView) findViewById(R.id.listView);
        activityList = new ArrayList<>();

        showActivites();
    }

    private void showActivites() {

        Cursor cursor = database.rawQuery("SELECT * FROM activities", null);

        if (cursor.moveToFirst()) {
            do {
                activityList.add(new Task(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2)
                ));
            } while (cursor.moveToNext());
            adapter = new TaskAdapter(this, R.layout.list_layout_activity, activityList);

            listView.setAdapter(adapter);



        }
        cursor.close();



    }

}
