package com.example.ewa.organize;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    SQLiteDatabase database;
    public static final String DATABASE_NAME = "database";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        database = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
        createActivitiesTable();
        Button addNewBtn = (android.widget.Button) findViewById(R.id.newActivityBtn);
                addNewBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent addActivityIntent = new Intent(getApplicationContext(), AddActivity.class);
                        startActivity(addActivityIntent);
                    }
                });

        Button showActivities = (Button) findViewById(R.id.showActivitiesBtn);
        showActivities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent showActivitiesIntent = new Intent(getApplicationContext(), ShowActivities.class);
                startActivity(showActivitiesIntent);
            }
        });
    }

    private void createActivitiesTable() {
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS activities (\n" +
                        "    ID INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
                        "    name varchar(200) NOT NULL,\n" +
                        "    date varchar(200) NOT NULL\n" +
                        ");"
        );
    }
}
