package com.example.ewa.organize;

import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddActivity extends AppCompatActivity {

    SQLiteDatabase database;
    Button saveButton;
    EditText actionNameEditText;
    EditText actionDateEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        saveButton = (Button) findViewById(R.id.saveButton);
        actionNameEditText = (EditText) findViewById(R.id.actionName);
        actionDateEditText = (EditText) findViewById(R.id.actionDate);
        final AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Dodano czynność. Wróć do menu albo dodaj kolejną");
        builder1.setCancelable(false);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = actionNameEditText.getText().toString().trim();
                String date = actionDateEditText.getText().toString().trim();
                database = openOrCreateDatabase(MainActivity.DATABASE_NAME, MODE_PRIVATE, null);
                String insertSQL = "INSERT INTO activities \n" +
                        "(name, date)\n" +
                        "VALUES \n" +
                        "(?, ?);";
                database.execSQL(insertSQL, new String[]{name, date});


                builder1.setPositiveButton(
                        "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
                actionNameEditText.setText("");
                actionDateEditText.setText("");

            }
        });
    }


}
