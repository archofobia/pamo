package adapters;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.ewa.organize.R;

import java.util.List;

import models.Task;

public class TaskAdapter extends ArrayAdapter<Task> {

    int listLayoutRes;
    Context c;
    List<Task> activityList;
    SQLiteDatabase database;

    public TaskAdapter(@NonNull Context c, int listLayoutRes, @NonNull List<Task> activityList) {
        super(c, listLayoutRes, activityList);

        this.c=c;
        this.listLayoutRes =listLayoutRes;
        this.activityList=activityList;
        this.database=database;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater mInflater = LayoutInflater.from(c);
        View v = mInflater.inflate(listLayoutRes, null);

        Task activity = activityList.get(position);

        TextView nameTextView = (TextView) v.findViewById(R.id.aName);
        TextView dateTextView = (TextView) v.findViewById(R.id.aDate);

        nameTextView.setText(activityList.get(position).getName());
        dateTextView.setText(activity.getDate());

        return v;
    }
}
